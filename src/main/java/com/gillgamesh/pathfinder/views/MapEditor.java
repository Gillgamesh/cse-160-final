package com.gillgamesh.pathfinder.views;

import com.gillgamesh.pathfinder.controllers.EditorController;
import com.gillgamesh.pathfinder.views.fxml.EditorFXMLWrapper;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.io.IOException;

public class MapEditor extends VBox {
    private FXMap map;
    private Slider costSlider;
    private Button saveButton;
    private Text brushInfo;
    private ToggleGroup toggleGroup;
    private GridPane toolkit;

    private RadioButton wall;
    private RadioButton path;
    private RadioButton start;
    private RadioButton end;

    public MapEditor(int width, int height) {
        super();
        map = new FXMap(width, height);
        costSlider = new Slider();
        saveButton = new Button("Save");
        brushInfo = new Text();
        toggleGroup = new ToggleGroup();
        toolkit = new GridPane();
        brushInfo.setText("foobar");

        costSlider.setBlockIncrement(0.025);
        costSlider.setMin(0.0);
        costSlider.setMax(1.0);
        costSlider.setValue(0.5);
        wall = new RadioButton("Wall");
        start = new RadioButton("Start");
        end = new RadioButton("End");
        path = new RadioButton("Path");
        wall.setToggleGroup(toggleGroup);
        start.setToggleGroup(toggleGroup);
        end.setToggleGroup(toggleGroup);
        path.setToggleGroup(toggleGroup);
        path.setSelected(true);
        this.alignmentProperty().set(Pos.TOP_CENTER);
        this.spacingProperty().set(25);
        this.getChildren().addAll(
                map,
                brushInfo,
                toolkit);
        toolkit.alignmentProperty().set(Pos.TOP_CENTER);
        toolkit.add(costSlider,1,1);
        toolkit.add(wall,2,1);
        toolkit.add(path,2,2);
        toolkit.add(start,2,3);
        toolkit.add(end,2,4);
        toolkit.add(saveButton,2,5);
        this.prefWidth(640);
        this.setPrefHeight(640);
        map.setPrefWidth(640);
        map.setPrefHeight(480);


    }

    public FXMap getMap() {
        return map;
    }

    public Slider getCostSlider() {
        return costSlider;
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public Text getBrushInfo() {
        return brushInfo;
    }
    public ToggleGroup getToggleGroup() {
        return toggleGroup;
    }

    public EditorController.Mode currentBrushMode() {
        if (toggleGroup.getSelectedToggle().equals(path))
            return EditorController.Mode.PATH;
        if (toggleGroup.getSelectedToggle().equals(wall))
            return EditorController.Mode.WALL;
        if (toggleGroup.getSelectedToggle().equals(start))
            return EditorController.Mode.START;
        return EditorController.Mode.END;
    }
}
