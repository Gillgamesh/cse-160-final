package com.gillgamesh.pathfinder.views.fxml;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class EditorFXMLWrapper {
    @FXML
    public Pane mapView;


    @FXML
    public RadioButton wall;

    @FXML
    public RadioButton path;

    @FXML
    public RadioButton start;

    @FXML
    public RadioButton end;

    @FXML
    public Button saveButton;

    @FXML
    public Slider costSlider;

    @FXML
    public Text brushInfo;

    @FXML
    public void initialize() {

    }
}
