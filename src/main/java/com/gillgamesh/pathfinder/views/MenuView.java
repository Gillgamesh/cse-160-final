package com.gillgamesh.pathfinder.views;

import com.gillgamesh.pathfinder.App;
import com.gillgamesh.pathfinder.util.Point;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.function.UnaryOperator;

public class MenuView extends VBox {
    private ComboBox dropdown;
    private Button newMapButton;
    private Button editMapButton;
    private Button simulateMapButton;
    private Button randomMapButton;
    private TextField x;
    private TextField y;

    public MenuView() {

        this.spacingProperty().set(25);
        this.alignmentProperty().set(Pos.TOP_CENTER);
        this.setPadding(new Insets(25,25,25,25));
        dropdown = new ComboBox();
        dropdown.getItems().addAll(
                "Dijkstra (SP)",
                "Taxicab A* (SP)",
                "DFS",
                "BFS"
        );
        dropdown.setValue("Dijkstra (SP)");
        simulateMapButton = new Button("Load And Simulate");
        randomMapButton = new Button("Random Map");
        newMapButton = new Button("Create New Map");
        editMapButton = new Button("Edit Existing Map");
        GridPane gridPane = new GridPane();
        Text message = new Text(10,20,"Pathfinder");
        message.setFont(Font.font("Verdana", 24));
        message.setStyle(
                "padding:30px;"
        );
        this.getChildren().addAll(
                message,
                dropdown,
                simulateMapButton,
                randomMapButton,
                new Separator(),
                editMapButton,
                gridPane,
                newMapButton
        );

        x = new TextField();
        y = new TextField();
        gridPane.setHgap(5);
        gridPane.setVgap(5);
        gridPane.setAlignment(Pos.TOP_CENTER);
        gridPane.add(new Label("X"), 1,1);
        gridPane.add(x, 2, 1);
        gridPane.add(new Label("Y"), 1, 2);
        gridPane.add(y,2,2);

        UnaryOperator<TextFormatter.Change> filter = (t) -> {
            if (t.getText().matches("([0-9])*"))
                return t;
            t.setText("");
            return t;
        };
        x.setTextFormatter(new TextFormatter<>(filter));
        y.setTextFormatter(new TextFormatter<>(filter));

    }

    public int getNewMapWidth() {
        return Integer.parseInt(x.getText());
    }
    public int getNewMapHeight() {
        return Integer.parseInt(y.getText());
    }


    public App.SolverType getSolverType() {
        if (((String) dropdown.getValue()).equals("Dijkstra (SP)"))
            return App.SolverType.DIJKSTRA;
        if (dropdown.getValue().equals("DFS"))
            return App.SolverType.DFS;
        if (dropdown.getValue().equals("BFS"))
            return App.SolverType.BFS;
        return App.SolverType.ASTAR;
    }

    public Button getEditMapButton() {
        return editMapButton;
    }

    public Button getNewMapButton() {
        return newMapButton;
    }

    public Button getRandomMapButton() {
        return randomMapButton;
    }

    public Button getSimulateMapButton() {
        return simulateMapButton;
    }
}
