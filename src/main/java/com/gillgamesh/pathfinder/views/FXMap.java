package com.gillgamesh.pathfinder.views;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class FXMap extends Pane {


    private int mapWidth;
    private int mapHeight;

    public FXMap(int width, int height) {
        super();
        this.mapWidth = width;
        this.mapHeight = height;
        //setup rectangles:
        for (int y =0; y < height; y++) {
            for (int x=0; x< width; x++) {
                Rectangle rectangle = new Rectangle();
                rectangle.xProperty().bind(
                        this.widthProperty().multiply(x).divide(width));
                rectangle.yProperty().bind(
                        this.heightProperty().multiply(y).divide(height));
                rectangle.widthProperty().bind(
                        this.widthProperty().divide(width));
                rectangle.heightProperty().bind(
                        this.heightProperty().divide(height));
                rectangle.setFill(Color.WHITE);
                rectangle.setStroke(Color.BLACK);
                this.getChildren().add(y*width+x, rectangle);
            }
        }

    }

    public Rectangle getRectangle(int x, int y) {
        return (Rectangle) (this.getChildren().get(y*mapWidth+x));
    }
    public Color getColor(int x, int y) {
        return (Color) (getRectangle(x,y).getFill());
    }
    public Color setColor(int x, int y, Color color) {
        Color old = getColor(x,y);
        getRectangle(x,y).setFill(color);
        return old;
    }

    public Color setColor(int x, int y, double cost) {
        double greyVal = 1-cost;
        greyVal+=0.25;
        greyVal/=1.25;
        if (greyVal < 0)
            greyVal = 0;
        if (greyVal >= 1)
            greyVal=1;
        return setColor(x,y, Color.color(greyVal,greyVal,greyVal));
    }

}
