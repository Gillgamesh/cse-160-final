package com.gillgamesh.pathfinder.controllers;

import com.gillgamesh.pathfinder.App;
import com.gillgamesh.pathfinder.map.GridBlock;
import com.gillgamesh.pathfinder.map.MapData;
import com.gillgamesh.pathfinder.solvers.PathSolver;
import com.gillgamesh.pathfinder.solvers.astar.AStarDistance;
import com.gillgamesh.pathfinder.util.MapFile;
import com.gillgamesh.pathfinder.util.Point;
import com.gillgamesh.pathfinder.views.FXMap;
import com.gillgamesh.pathfinder.views.MapEditor;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.scene.control.Toggle;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class EditorController extends MapController {
    public enum Mode {
        WALL, PATH, START, END
    }
    private MapEditor editorView;
    private DoubleProperty sliderVal;
    public EditorController(App app, MapEditor editor, MapData mapData) {
        super(app, editor.getMap(), mapData);
        this.editorView = editor;
        sliderVal = editorView.getCostSlider().valueProperty();

        //set status binding:
        editorView.getBrushInfo().textProperty()
                .bind(Bindings.createStringBinding(
                        () -> {
                            return String.format("Cost: %.2f",
                                    sliderVal.get());
                        },
                        sliderVal)
                        );


        mapView.setOnMouseClicked(
                e -> {
                    Mode paintStyle = editorView
                            .currentBrushMode();
                    Point p = getCorrespondingPoint(
                            e.getX(),
                            e.getY()
                    );
                    switch (paintStyle) {
                        case PATH:
                            colorInPath(p);
                            break;
                        case WALL:
                            wallInBlock(p);
                            break;
                        case START:
                            newStartPoint(p);
                            break;
                        case END:
                            newEndPoint(p);
                            break;
                    }
                }
        );
        mapView.setOnMouseMoved(
                e -> {
                    if (e.isControlDown())
                    mapView.getOnMouseClicked().handle(e);
                }
        );
        editorView.getSaveButton().setOnMouseClicked(
                e -> {
                    if (mapData.getStartPoint() == null
                            || mapData.getEndPoint() == null) {
                     //notify user that they're messing start and end
                        System.err.println("No start or end point!");
                        return;
                    }
                    PathSolver<Point> solver = new AStarDistance(mapData);
                    if (solver.findPath() == null) {
                        //notify the user that there isn't a path
                        //in the updated data
                        System.err.println("No path exists!");
                        return;
                    }
                    FileChooser chooser = new FileChooser();
                    chooser.setTitle("Save Map");
                    File file = chooser.showSaveDialog(new Stage());
                    if (file == null) {
                        System.err.println("File not selected?");
                        return;
                    }
                    if (!file.exists()) try {
                        file.createNewFile();
                    } catch (IOException exception) {
                        System.err.println(
                                "Unable to create new file!");
                    }
                    if (!file.canWrite()) {
                        System.err.println("File cannot be written to!");
                        return;
                    }
                    System.out.println(file);
                    MapFile.writeCostMatrix(file, mapData);
                }
        );
    }

    private void newStartPoint(Point p) {
        //remove old start point: keep it's old cost value
        Point old = mapData.getStartPoint();
        if (old !=null)
            mapView.setColor(old.getX(),
                    old.getY(),
                    mapData.get(old).getCost());
        mapView.setColor(p.getX(),
                p.getY(),
                Color.GREEN);
        if (!mapData.get(p).isWalkable()) {
            mapData.get(p).setWalkable(true);
            mapData.get(p).setCost(0.0);
        }
        mapData.setStartPoint(p);
    }
    private void newEndPoint(Point p) {
        //remove old start point: keep it's old cost value
        Point old = mapData.getEndPoint();
        if (old !=null)
            mapView.setColor(old.getX(),
                    old.getY(),
                    mapData.get(old).getCost());
        mapView.setColor(p.getX(),
                p.getY(),
                Color.RED);
        if (!mapData.get(p).isWalkable()) {
            mapData.get(p).setWalkable(true);
            mapData.get(p).setCost(0.0);
        }
        mapData.setEndPoint(p);
    }
    private void wallInBlock(Point p) {
        GridBlock block = mapData.get(p);
        block.setWalkable(false);
        if (block.getCost() == 0)
            block.setCost(-1.0);
        else if (block.getCost() > 0)
            block.setCost(-1*block.getCost());
        mapView.setColor(p.getX(),p.getY(),
                Color.BLACK);
        //make sure it is not start or end
        if (mapData.getStartNode() != null
                && p.equals(mapData.getStartNode()))
            mapData.setStartPoint(null);
        if (mapData.getStartNode() != null
                && p.equals(mapData.getEndNode()))
            mapData.setEndPoint(null);

    }

    private void colorInPath(Point p) {

        mapData.get(p).setCost(
                sliderVal.get()
        );
        mapView.setColor(
                p.getX(),
                p.getY(),
                sliderVal.get()
        );
        //de-wall it
        mapData.get(p)
                .setWalkable(true);
        //de-start and de-end
        if (mapData.getStartNode() != null
                && p.equals(mapData.getStartNode()))
            mapData.setStartPoint(null);
        if (mapData.getStartNode() != null
                && p.equals(mapData.getEndNode()))
            mapData.setEndPoint(null);
    }
    private Point getCorrespondingPoint(double x, double y) {
        int xConverted = (int) (x/mapView.getWidth()
                * mapData.getWidth());

        int yConverted = (int) (y/mapView.getHeight()
                * mapData.getHeight());
        if (xConverted >= mapData.getWidth())
            xConverted = mapData.getWidth()-1;
        if (yConverted >= mapData.getHeight())
            yConverted = mapData.getHeight()-1;
        return new Point(xConverted, yConverted);
    }

}
