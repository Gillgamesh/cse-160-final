package com.gillgamesh.pathfinder.controllers;

import com.gillgamesh.pathfinder.App;
import com.gillgamesh.pathfinder.views.MenuView;
import javafx.application.Application;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class MainMenuController {
    private MenuView view;
    private App app;
    public MainMenuController(App app, MenuView view) {
        this.app = app;
        this.view = view;

        view.getEditMapButton().setOnMouseClicked(
                e -> {
                    FileChooser chooser = new FileChooser();
                    chooser.setTitle("Load Map");
                    File file = chooser.showOpenDialog(new Stage());
                    if (file != null)
                        app.startEditorFile(new Stage(), file);
                }
        );
        view.getNewMapButton().setOnMouseClicked(
                e -> {
                    app.startNewEditor(
                            new Stage(),
                            view.getNewMapWidth(),
                            view.getNewMapHeight()
                            );
                }
        );

        view.getSimulateMapButton().setOnMouseClicked(
                e -> {
                    FileChooser chooser = new FileChooser();
                    chooser.setTitle("Load Map");
                    File file = chooser.showOpenDialog(new Stage());
                    App.SolverType solver = view.getSolverType();
                    if (file != null)
                        app.startSimulation(
                                new Stage(),
                                file,
                                solver
                        );

                }
        );
        view.getRandomMapButton().setOnMouseClicked(
                e -> {
                    App.SolverType solver = view.getSolverType();
                    app.startRandomSimulation(
                            new Stage(),
                            solver
                    );
                }
        );

    }
}
