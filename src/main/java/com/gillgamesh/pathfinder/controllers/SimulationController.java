package com.gillgamesh.pathfinder.controllers;

import com.gillgamesh.pathfinder.App;
import com.gillgamesh.pathfinder.map.MapData;
import com.gillgamesh.pathfinder.map.MapRenderer;
import com.gillgamesh.pathfinder.solvers.PathSolver;
import com.gillgamesh.pathfinder.util.Point;
import com.gillgamesh.pathfinder.util.Tuple;
import com.gillgamesh.pathfinder.views.FXMap;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

public class SimulationController extends MapController
implements MapRenderer {

    private PathSolver<Point> solver;
    private List<Point> finalPath;
    private List<Point> allVisited;
    private double stepTime = 0.5;
    private int trailLength = 10;

    public SimulationController (App app,
                         FXMap mapView,
                         MapData mapData,
                         PathSolver<Point> solver) {
        super(app,mapView,mapData);
        this.solver = solver;
    }

    public boolean findPath() {
        Tuple<List<Point>, List<Point>> path = solver.showStepsToPath();
        if (path == null)
            return false;
        finalPath = path.getSecond();
        allVisited = path.getFirst();
        return true;
    }

    @Override
    public void showPathFinding() {
        getPathFindingTL().play();
    }

    public Timeline getPathFindingTL() {
        Iterator<Point> iterator = allVisited.iterator();
        final Deque<Point> trail = new LinkedList<>();
        Timeline timeline = new Timeline(
                new KeyFrame(
                        Duration.seconds(stepTime),
                        (event) -> {
                            Point previous = (trail.size() == 0) ? null : trail.getLast();
                            if (previous != null)
                                mapView.setColor(
                                        previous.getX(),
                                        previous.getY(),
                                        Color.CYAN
                                );
                            Point p = iterator.next();
                            if (p.equals(mapData.getStartNode())
                                    || p.equals(mapData.getEndNode()))
                                return;
                            mapView.setColor(
                                    p.getX(),
                                    p.getY(),
                                    Color.BLUE
                            );
                            trail.offer(p);
                            while (trail.size() > trailLength) {
                                Point oldestInTrail = trail.poll();
                                mapView.setColor(
                                        oldestInTrail.getX(),
                                        oldestInTrail.getY(),
                                        Color.YELLOW
                                );
                            }
                        }
                )
        );
        timeline.setCycleCount(allVisited.size());
        return timeline;
    }


    private void showNext(Iterator<Point> iterator, Color color) {
        if (!iterator.hasNext())
            return;
        Point p = iterator.next();
        PauseTransition delay = new PauseTransition(
                Duration.seconds(stepTime)
        );
        delay.setOnFinished(
                (event) -> {
                    if (!p.equals(mapData.getStartNode())
                    && !p.equals(mapData.getEndNode()))
                    mapView.setColor(
                            p.getX(),
                            p.getY(),
                            color
                    );
                    showNext(iterator, color);
                }
        );
        delay.play();
    }

    @Override
    public void walkPath() {
        getwalkPathTL().play();
    }

    public Timeline getwalkPathTL() {
        Iterator<Point> iterator = finalPath.iterator();
        Timeline timeline = new Timeline(
                new KeyFrame(
                        Duration.seconds(stepTime),
                        (event) -> {
                            Point p = iterator.next();
                            if (p.equals(mapData.getStartNode())
                            || p.equals(mapData.getEndNode()))
                                return;
                            mapView.setColor(
                                    p.getX(),
                                    p.getY(),
                                    Color.BLUE
                            );
                        }
                )
        );
        timeline.setCycleCount(finalPath.size());
        return timeline;
    }

}
