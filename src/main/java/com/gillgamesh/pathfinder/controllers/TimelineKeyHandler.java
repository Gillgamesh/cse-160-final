package com.gillgamesh.pathfinder.controllers;

import javafx.animation.Animation;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class TimelineKeyHandler implements EventHandler<KeyEvent> {

    private Timeline timeline;
    private Stage stage;
    public TimelineKeyHandler(Timeline timeline) {
        this.timeline = timeline;
    }
    public TimelineKeyHandler(Timeline timeline, Stage stage) {
        this(timeline);
        this.stage = stage;
    }
    @Override
    public void handle(KeyEvent keyEvent) {
        switch (keyEvent.getCode()) {
            case SPACE:
                if (timeline.getStatus()
                        .equals(Animation.Status.PAUSED))
                    timeline.play();
                else
                    timeline.pause();
                break;
            case K:
                timeline.setRate(timeline.getRate()*1.5);
                break;
            case J:
                timeline.setRate(timeline.getRate()/1.5);
                break;
            case ESCAPE:
                if (stage != null)
                    stage.close();
        }
    }
}
