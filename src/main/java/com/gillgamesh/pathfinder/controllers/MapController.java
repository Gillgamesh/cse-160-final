package com.gillgamesh.pathfinder.controllers;

import com.gillgamesh.pathfinder.App;
import com.gillgamesh.pathfinder.map.GridBlock;
import com.gillgamesh.pathfinder.map.MapData;
import com.gillgamesh.pathfinder.map.MapRenderer;
import com.gillgamesh.pathfinder.solvers.PathSolver;
import com.gillgamesh.pathfinder.util.Point;
import com.gillgamesh.pathfinder.util.Tuple;
import com.gillgamesh.pathfinder.views.FXMap;
import javafx.scene.paint.Color;

import java.util.List;

public class MapController {

    protected App application;
    protected FXMap mapView;
    protected MapData mapData;

    public MapController(App app,
                         FXMap mapView,
                         MapData mapData) {
        this.application = app;
        this.mapView = mapView;
        this.mapData = mapData;
    }


    // rerender the map from scratch based off the data
    public void renderMap() {
        for (int i =0; i < mapData.getWidth(); i++) {
            for (int j=0; j < mapData.getHeight(); j++) {
                GridBlock block = mapData.get(i,j);
                if (!block.isWalkable())
                    mapView.setColor(i,j, Color.BLACK);
                else if (mapData.getStartPoint() != null &&
                        mapData.getStartPoint().equals(
                        new Point(i,j)
                ))
                    mapView.setColor(i,j,Color.GREEN);
                else if (mapData.getEndPoint() != null &&
                        mapData.getEndPoint().equals(
                        new Point(i,j)
                ))
                    mapView.setColor(i,j,Color.RED);
                else {
                    mapView.setColor(i, j, block.getCost());
                }
            }
        }
    }

    protected double clip(double x, double min, double max) {
        return Math.max(
                Math.min(x, max),
                min);
    }

}
