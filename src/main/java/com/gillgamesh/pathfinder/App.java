package com.gillgamesh.pathfinder;

import com.gillgamesh.pathfinder.controllers.EditorController;
import com.gillgamesh.pathfinder.controllers.MainMenuController;
import com.gillgamesh.pathfinder.controllers.SimulationController;
import com.gillgamesh.pathfinder.controllers.TimelineKeyHandler;
import com.gillgamesh.pathfinder.map.MapData;
import com.gillgamesh.pathfinder.solvers.PathSolver;
import com.gillgamesh.pathfinder.solvers.astar.AStarDistance;
import com.gillgamesh.pathfinder.solvers.astar.BFS;
import com.gillgamesh.pathfinder.solvers.astar.DFS;
import com.gillgamesh.pathfinder.solvers.astar.Dijkstra;
import com.gillgamesh.pathfinder.util.DataFormatter;
import com.gillgamesh.pathfinder.util.MapFile;
import com.gillgamesh.pathfinder.util.Point;
import com.gillgamesh.pathfinder.views.FXMap;
import com.gillgamesh.pathfinder.views.MapEditor;
import com.gillgamesh.pathfinder.views.MenuView;
import com.gillgamesh.pathfinder.views.fxml.EditorFXMLWrapper;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.File;
import java.util.Map;


public class App extends Application {
    public enum SolverType {
        DIJKSTRA,
        ASTAR,
        DFS,
        BFS
    }

    public static void main(String[] args) {
        launch(args);
    }
    private MapData data;
    private FXMap view;
    @Override
    public void start(Stage primaryStage) {
        MenuView mainMenu = new MenuView();
        MainMenuController controller = new MainMenuController(this, mainMenu);
        Scene scene = new Scene(mainMenu);
        primaryStage.setScene(scene);
        primaryStage.setWidth(480);
        primaryStage.setHeight(480);
        primaryStage.setTitle("Pathfinder");
        primaryStage.show();
    }

    public void startNewEditor(Stage stage, int width, int height) {
        startEditor(stage, MapData.emptyMap(width, height));
    }
    public void startEditor(Stage stage, MapData mapData) {
        data = mapData;
        MapEditor editorView = new MapEditor(
                data.getWidth(),
                data.getHeight()
        );
        this.view = editorView.getMap();

        EditorController controller = new EditorController(
                this,
                editorView,
                data
        );
        controller.renderMap();

        Scene scene = new Scene(editorView);
        stage.setWidth(640);
        stage.setHeight(640);
        stage.setScene(scene);
        stage.show();

    }
    public void startEditorFile(Stage stage, File file) {
        data = MapFile.readCostMatrix(file);
        if (data == null) {
            System.err.println("ERROR: File not loaded properly!");
            return;
        }
        startEditor(stage, data);
    }



    public void startRandomSimulation(Stage stage, SolverType type) {
        double[][] matrix = new double[25][25];
        for (int i=0; i < matrix.length; i++) {
            for (int j=0; j < matrix[0].length; j++) {
                matrix[i][j] = Math.random();
                // a tenth will be walls
                if (Math.random() < .1) {
                    matrix[i][j]*=-1;
                }
            }
        }
        matrix[0][0] = 0.0;
        matrix[24][24]=0.0;
        MapData data = DataFormatter.fromCostMatrix(
                matrix,
                new Point(0,0),
                new Point(24,24)
        );
        if (data.isSolvable())
            startSimulation(stage, data, type);
        else
            startRandomSimulation(stage,type);

    }

    public void startSimulation(Stage stage, MapData data, SolverType type) {
        PathSolver<Point> solver;
        this.data = data;
        if (type == SolverType.ASTAR) {
            solver = new AStarDistance(data);
        } else if (type == SolverType.DFS) {
            solver = new DFS<>(data);
        } else if (type == SolverType.BFS) {
            solver = new BFS<>(data);
        }
        else {
            solver = new Dijkstra<>(data);
        }
        view = new FXMap(
                data.getWidth(),
                data.getHeight()
        );
        SimulationController controller = new SimulationController(
                this,
                view,
                data,
                solver
        );
        Scene scene = new Scene(view);
        stage.setScene(scene);
        stage.show();
        stage.setWidth(640);
        stage.setHeight(stage.getWidth()*
                (((double)data.getHeight())
                        /data.getWidth()));
        controller.renderMap();
        if (controller.findPath()) {
            Timeline pathFinder = controller.getPathFindingTL();
            pathFinder.setOnFinished(
                    (event) -> {
                        Timeline walk = controller.getwalkPathTL();
                        scene.setOnKeyPressed(
                                new TimelineKeyHandler(walk, stage)
                        );
                        walk.play();
                    }
            );
            pathFinder.play();
            pathFinder.pause();
            scene.setOnKeyPressed(
                    new TimelineKeyHandler(pathFinder, stage)
            );
        } else {

        }
    }
    public void startSimulation(Stage stage, File file, SolverType type  ) {
        PathSolver<Point> solver;
        data = MapFile.readCostMatrix(file);
        if (data == null) {
            System.err.println("ERROR: File not loaded properly!");
            return;
        }
        startSimulation(stage, data, type);
    }
}
