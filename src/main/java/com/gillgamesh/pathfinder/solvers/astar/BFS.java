package com.gillgamesh.pathfinder.solvers.astar;

import com.gillgamesh.pathfinder.solvers.PathSolver;
import com.gillgamesh.pathfinder.util.Tuple;
import com.gillgamesh.pathfinder.util.WeightedGraph;

import java.util.*;

public class BFS<N> implements PathSolver<N> {
    private WeightedGraph<N> graph;
    private Map<N,N> cameFrom;
    private List<N> discoveredNodesOrder;
    private Queue<N> toExplore;



    public BFS(WeightedGraph<N> graph) {
        this.graph = graph;
        cameFrom = new HashMap<>();
        this.discoveredNodesOrder = new ArrayList<>();
        this.toExplore = new LinkedList<>();
    }

    @Override
    public Tuple<List<N>, List<N>> showStepsToPath() {
        toExplore.offer(graph.getStartNode());
        cameFrom.put(graph.getStartNode(), null);
        while (!toExplore.isEmpty()) {
            N currentNode = toExplore.poll();
            discoveredNodesOrder.add(currentNode);
            Set<N> neighbors = graph.getWeightedEdges(currentNode).keySet();
            for (N neighbor: neighbors) {
                if (!cameFrom.containsKey(neighbor)) {
                    toExplore.offer(neighbor);
                    cameFrom.put(neighbor, currentNode);
                    if (neighbor.equals(graph.getEndNode())) {
                        return new Tuple<>(discoveredNodesOrder, constructPath());
                    }
                }
            }
        }
        return null;
    }

    private List<N> constructPath() {
        LinkedList<N> path = new LinkedList<>();
        N current = graph.getEndNode();
        do {
            path.addFirst(current);
            current = cameFrom.get(current);
        } while (current != null);
        return path;
    }
}
