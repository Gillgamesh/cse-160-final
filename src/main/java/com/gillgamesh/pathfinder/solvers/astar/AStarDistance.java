package com.gillgamesh.pathfinder.solvers.astar;

import com.gillgamesh.pathfinder.util.Point;
import com.gillgamesh.pathfinder.util.WeightedGraph;

public class AStarDistance extends AStar<Point> {

    public AStarDistance(WeightedGraph<Point> graph) {
        super(graph);
    }
    @Override
    public int heuristic(Point node1, Point node2) {
        //since we are int rounding, we multiply by 10 to match the cost
        return (int) Math.max((50*node1.distance(node2,1)),0);
    }
}
