package com.gillgamesh.pathfinder.solvers.astar;

import com.gillgamesh.pathfinder.solvers.PathSolver;
import com.gillgamesh.pathfinder.util.Point;
import com.gillgamesh.pathfinder.util.Tuple;
import com.gillgamesh.pathfinder.util.WeightedGraph;

import java.util.*;

public abstract class AStar<N> implements PathSolver<N> {

    public abstract int heuristic(N node1, N node2);

    private WeightedGraph<N> graph;
    private PriorityQueue<N> openSet;
    private Set<N> closedSet;
    //the cost without the heuristic-ie the actual cost
    private Map<N, Integer> costsFromStart;

    //cost including the heuristic
    private Map<N, Integer> totalCostsFromStart;
    //keep track of what node a node came from for it's quickest route
    //from the start node.
    private Map<N, N> cameFrom;

    public AStar(WeightedGraph<N> graph) {
        this.graph =graph;
        this.cameFrom = new HashMap<>();
        this.costsFromStart = new HashMap<>();
        this.totalCostsFromStart = new HashMap<>();
        this.closedSet = new HashSet<>();
        Set<N> allNodes = graph.getAllNodes();
        System.out.println(allNodes);

        for (N node : allNodes) {
            costsFromStart.put(node, Integer.MAX_VALUE);
            totalCostsFromStart.put(node, Integer.MAX_VALUE);
        }

        Comparator<N> comparator = (n,t1) -> {
            if (!totalCostsFromStart.containsKey(n) )
                return -1;
            if (!totalCostsFromStart.containsKey(t1))
                return 1;
            return (totalCostsFromStart.get(n) - totalCostsFromStart.get(t1));

        };

        this.openSet = new PriorityQueue<>(allNodes.size(),
                      comparator);
    }

    @Override
    public List<N> findShortestPath() {
        Tuple<List<N>,List<N>> steps = showStepsToPath();
        if (steps == null)
            return null;
        return steps.getB();
    }

    @Override
    public Tuple<List<N>, List<N>> showStepsToPath() {
        List<N> allVisited = new LinkedList<>();
        N startNode = graph.getStartNode();
        N endNode = graph.getEndNode();

        costsFromStart.put(startNode, 0);
        totalCostsFromStart.put(startNode,
                heuristic(startNode,
                        endNode));

        openSet.add(graph.getStartNode());

        N current;
        while (!openSet.isEmpty()) {
            current = openSet.poll();
            allVisited.add(current);
            closedSet.add(current);

            if (current.equals(endNode)) {
//                System.out.println(costsFromStart.get(current));
                return new Tuple<>(allVisited, constructPath(current));
            }

            Map<N,Integer> neighbors = graph.getWeightedEdges(current);
//            System.out.println(costsFromStart);
            for (N neighbor: neighbors.keySet()) {
                // if a neighbor is in the closed set, a shorter path
                // alreacameFromdy led to it being on top of the priority queue. ignore it.
                if (!closedSet.contains(neighbor)) {
                    int currentToNeighborCost = neighbors.get(neighbor);
                    int newActualCost = costsFromStart.get(current)
                            + currentToNeighborCost;
                    if (!openSet.contains(neighbor)) {
                        openSet.add(neighbor);
                    }
                    if (newActualCost < costsFromStart.get(neighbor)) {
                        //either this is the first time we've found this node,
                        //or it's the the new fastest path
                        //even if it's our first time discovering the node, a cost
                        //of Integer.MAX_VALUE already exists in the map.
                        cameFrom.put(neighbor, current);
                        costsFromStart.put(neighbor,newActualCost);
                        totalCostsFromStart.put(neighbor,
                                newActualCost+heuristic(neighbor,endNode));
                    }
                }
            }
        }
        //if we get here, there is no possible path. return null
        return null;
    }
    public List<N> findPath() {
        return findShortestPath();
    }


    private List<N> constructPath(N current) {
        LinkedList<N> actualSteps = new LinkedList<>();
        while (cameFrom.containsKey(current)) {
            actualSteps.addFirst(current);
            current = cameFrom.get(current);
        }
        return actualSteps;
    }




}
