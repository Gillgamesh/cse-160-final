package com.gillgamesh.pathfinder.solvers.astar;

import com.gillgamesh.pathfinder.util.WeightedGraph;

public class Dijkstra<N> extends AStar<N>{

    public Dijkstra(WeightedGraph<N> graph) {
        super(graph);
    }

    @Override
    public int heuristic(N node1, N node2) {
        return 0;
    }
}
