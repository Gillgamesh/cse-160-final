package com.gillgamesh.pathfinder.solvers;

import com.gillgamesh.pathfinder.util.Tuple;
import com.gillgamesh.pathfinder.util.WeightedGraph;

import java.util.List;

public interface PathSolver<N> {

    Tuple<List<N>, List<N>> showStepsToPath();

    default List<N> findShortestPath() {
        Tuple<List<N>,List<N>> steps = showStepsToPath();
        if (steps == null)
            return null;
        return steps.getB();
    }
    default List<N> findPath() {
        return findShortestPath();
    }
}
