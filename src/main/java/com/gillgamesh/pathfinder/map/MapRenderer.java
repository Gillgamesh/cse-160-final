package com.gillgamesh.pathfinder.map;

public interface MapRenderer {

    void renderMap();
    void showPathFinding();
    void walkPath();
}
