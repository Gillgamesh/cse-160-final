package com.gillgamesh.pathfinder.map;

import com.gillgamesh.pathfinder.solvers.PathSolver;
import com.gillgamesh.pathfinder.solvers.astar.AStarDistance;
import com.gillgamesh.pathfinder.util.DataFormatter;
import com.gillgamesh.pathfinder.util.WeightedGraph;
import com.gillgamesh.pathfinder.util.Point;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MapData implements WeightedGraph<Point> {

    private int height;
    private int width;
    private GridBlock[][] matrix;

    private Point startPoint;
    private Point endPoint;

    public MapData(GridBlock[][] matrix, Point start, Point end) {
        this.matrix = matrix;
        this.height = matrix.length;
        this.width = matrix[0].length;
        this.startPoint = start;
        this.endPoint = end;
    }

    public GridBlock get(int x, int y) {
        //coordinates will be based off the top-left being 0,0, in column,row order
        return matrix[y][x];
    }
    public GridBlock get(Point p) {
        return get(p.getX(), p.getY());
    }
    public int getHeight() {
        return height;
    }
    public int getWidth() {
        return width;
    }

    public Point setStartPoint(Point startPoint) {
        Point old = this.startPoint;
        this.startPoint = startPoint;
        return old;
    }
    public Point getStartPoint() {
        return startPoint;
    }

    public Point setEndPoint(Point endPoint) {
        Point old = this.endPoint;
        this.endPoint = endPoint;
        return old;
    }

    public Point getEndPoint() {
        return endPoint;
    }



    protected Set<Point> getNeighbors(Point point) {
        Set<Point> points = new HashSet<>();
        System.out.println(points);
        int x = point.getX();
        int y = point.getY();
        for (int i = x-1; i <= x+1; i++) {
            for (int j = y-1; j <= y+1; j++) {
                if (!(i == x && j == y)
                        && i >= 0 && j >= 0
                        && i < getWidth() && j < getHeight()
                        && get(i,j).isWalkable()
                        && (i == x || j == y))
                    points.add(new Point(i,j));
            }
        }
        return points;
    }


    @Override
    public Map<Point, Integer> getWeightedEdges(Point nodeIdentifier) {
        Map<Point, Integer> outputs = new HashMap<>();
        int x = nodeIdentifier.getX();
        int y = nodeIdentifier.getY();
        double cost = this.get(x,y).getCost();
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (!(i == x && j == y)
                        && i >= 0 && j >= 0
                        && i < getWidth() && j < getHeight()
                        && get(i, j).isWalkable()
                        && (i == x || j == y)) {
                    Point other = new Point(i,j);
                    outputs.put(other, travelTime(nodeIdentifier, other));
                }
            }
        }
        return outputs;
    }
    //extrapolates the "distance" between 2 points
    //assume that cost represents a slowing factor
    //ie x=vt, v=(1-cost+0.1).=>t=x/v
    // with cost=0, we will be going "full speed"
    //howeve,r we run into a problem of v=1.1
    //which means travel time (calculated distance) will
    //be less than 1 at 0, so we will artificially boost
    private int travelTime(Point p1, Point p2) {
        double b = 0.1;
        double a = 0.1;
        double costA = get(p1).getCost();
        double costB = get(p2).getCost();
        double timeA = p1.distance(p2)/2;
        double timeB = timeA;
        double velA = (1-costA+a);
        double velB = (1-costB+a);
        timeA *= 1/velA + b;
        timeB *= 1/velB + b;
        return (int) (100*(timeA+timeB));    }


    @Override
    public Set<Point> getAllNodes() {
        Set<Point> points = new HashSet<Point>();
        Point p;
        for (int i=0; i < getWidth(); i++) {
            for (int j=0; j < getHeight(); j++) {
                p = new Point(i,j);
                if (this.get(p).isWalkable())
                    points.add(p);
            }
        }
        return points;
    }

    @Override
    public Point getStartNode() {
        return this.startPoint;
    }

    @Override
    public Point getEndNode() {
        return this.endPoint;
    }

    @Override
    public int getNumNodes() {
        return getWidth()*getHeight();
    }

    public static MapData emptyMap(int width, int height) {
        double[][] data = new double[height][width];
        MapData emptyMap = DataFormatter
                .fromCostMatrix(data, null, null);
        return emptyMap;
    }

    public boolean isSolvable() {
        PathSolver<Point> solver = new AStarDistance(this);
        return solver.findPath() != null;
    }
}
