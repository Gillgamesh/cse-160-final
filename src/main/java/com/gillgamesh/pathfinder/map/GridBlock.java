package com.gillgamesh.pathfinder.map;

public class GridBlock {
    // TODO - decide whether to use awt.Color
    private boolean walkable;
    private double cost;

    public GridBlock(int[] color, boolean walkable, double cost) {
        this.walkable = walkable;
        this.cost = cost;
    }
    public boolean isWalkable() {
        return walkable;
    }
    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setWalkable(boolean walkable) {
        this.walkable = walkable;
    }

}
