package com.gillgamesh.pathfinder.util;

public class Tuple<A, B> {
    private A a;
    private B b;
    public Tuple(A a, B b) {
        this.a = a;
        this.b = b;
    }
    public A getA() {
        return a;
    }

    public A getFirst() {
        return getA();
    }
    public B getSecond() {
        return getB();
    }

    public B getB() {
        return b;
    }
}
