package com.gillgamesh.pathfinder.util;

public class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getY() {
        return y;
    }
    public int getX() {
        return x;
    }

    public int taxiNorm(Point other) {
        return Math.abs(this.getX() - other.getX())
                + Math.abs((this.getY() - other.getY()));
    }

    public double distance(Point other, double p) {
        double normP = Math.pow(Math.abs(
                this.getY() - other.getY()),
                p) +
                        Math.pow(Math.abs(
                                this.getX() - other.getX()),
                p);

        return Math.pow(
                normP, 1/p
        );
    }
    public double norm(Point other) {
        return this.distance(other, 2);
    }
    public double distance(Point other) {
        return this.norm(other);
    }

    @Override
    public int hashCode() {
        return x*1024 + y;
    }

    public boolean equals(Point other) {
        return this.x == other.x && this.y == other.y;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Point) {
            Point other = (Point)obj;
            return this.equals(other);
        }
        return false;
    }


    @Override
    public String toString() {
        return String.format("Point(%d, %d)",
                getX(),
                getY());
    }
}
