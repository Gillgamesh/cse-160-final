package com.gillgamesh.pathfinder.util.test;

import com.gillgamesh.pathfinder.map.MapData;
import com.gillgamesh.pathfinder.solvers.astar.AStar;
import com.gillgamesh.pathfinder.solvers.astar.Dijkstra;
import com.gillgamesh.pathfinder.util.DataFormatter;
import com.gillgamesh.pathfinder.util.Point;

import java.util.List;
import java.util.Map;

public class SimpleMap {
    public static void main(String[] args) {
        MapData simpleMap = DataFormatter.fromCostMatrix(
                new double[][] {
                        {0.5,-0.3,0.6,0.3,0.1},
                        {0.3,-0.2,0.2,0.2,0.1},
                        {0.1,0.5,-.2,0.2,0.1},
                        {0.1,-0.2,0.2,0.2,0.1},
                        {0.1,0.2,0.2,0.2,0.1},
                        {0.1,0.2,0.2,0.2,0.1},
                        {0.1,0.2,0.2,0.2,0.1},
                },
                new Point(0,0),
                new Point(3,2)
        );
        Dijkstra<Point> solver = new Dijkstra<>(simpleMap);
        System.out.println(solver.findShortestPath());

        AStar<Point> solver2 = new AStar<Point>(simpleMap) {
            @Override
            public int heuristic(Point node1, Point node2) {
                return (int) node1.distance(node2);
            }
        };
        System.out.println(solver2.findShortestPath());
    }
}
