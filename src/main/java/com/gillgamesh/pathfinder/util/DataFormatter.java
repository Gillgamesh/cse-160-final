package com.gillgamesh.pathfinder.util;

import com.gillgamesh.pathfinder.map.GridBlock;
import com.gillgamesh.pathfinder.map.MapData;


public class DataFormatter {

    public static double[][] toCostMatrix(MapData map) {
        // creates an exact clone with just the int values
        double[][] newMatrix = new double[map.getHeight()][map.getWidth()];
        for (int i =0; i < map.getHeight(); i++) {
            for (int j=0; j < map.getWidth(); j++) {
                newMatrix[i][j] = map.get(j,i).getCost();
            }
        }
        return newMatrix;
    }

    public static MapData fromCostMatrix(double[][] matrix,
                                         Point p1,
                                         Point p2) {
        GridBlock[][] gridBlocks = new GridBlock[matrix.length][matrix[0].length];
        double cost;
        for (int i =0; i < gridBlocks.length; i++) {
            for (int j=0; j < gridBlocks[0].length; j++) {
                cost = matrix[i][j];
                boolean walkable = (cost >= 0);
                gridBlocks[i][j] = new GridBlock(
                        new int[] {255,255,255},
                        walkable,
                        cost
                );
            }
        }
        return new MapData(gridBlocks, p1, p2);
    }
}
