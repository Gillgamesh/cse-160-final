package com.gillgamesh.pathfinder.util;

import java.util.Map;
import java.util.Set;

//N = Node
public interface WeightedGraph<N>{
    //returns the edges and their weights
    Map<N, Integer> getWeightedEdges(N nodeIdentifier);
    Set<N> getAllNodes();
    int getNumNodes();
    N getStartNode();
    N getEndNode();
}
