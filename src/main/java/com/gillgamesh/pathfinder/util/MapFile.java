package com.gillgamesh.pathfinder.util;

import com.gillgamesh.pathfinder.map.MapData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class MapFile {


    public static void writeCostMatrix(File file, MapData map) {
        if (!file.exists()) {
            try {
                if (!file.createNewFile()) {
                    System.err.println("Cannot create new file!");
                    return;
                }
            } catch (IOException e) {

            }
        }
        else if (!file.canWrite()) {
            System.err.println("Cannot write file! " + file.toString());
            return;
        }
        double[][] costMatrix = DataFormatter.toCostMatrix(map);
        try {
            PrintWriter writer = new PrintWriter(file);
            writer.println(String.format(
                    "%d %d",
                    map.getWidth(),
                    map.getHeight()
            ));
            Point start = map.getStartNode();
            writer.println(String.format(
                    "%d %d",
                    start.getX(),
                    start.getY()
                    )
            );
            Point end = map.getEndNode();
            writer.println(String.format(
                    "%d %d",
                    end.getX(),
                    end.getY()
            ));
            //begin writing actual data:
            for (int i = 0; i < map.getHeight(); i++) {
                for (int j=0; j < map.getWidth(); j++) {
                    writer.print(costMatrix[i][j]+" ");
                }
                writer.println();
            }
            writer.close();
        } catch (Exception e) {

        }
    }

    public static MapData readCostMatrix(File file) {
        //should probably throw an exception but w/e
        if (!file.canRead())
            return null;
        Scanner scanner;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            return null;
        }
        String line = scanner.nextLine();
        String[] widthHeight = line.split("\\s");
        if (widthHeight.length !=2 ) {
            System.err.println("File Read Error: width height line");
            return null;
        }
        int width = Integer.parseInt(widthHeight[0]);
        int height = Integer.parseInt(widthHeight[1]);

        line = scanner.nextLine();
        String[] startCoords = line.split("\\s");
        if (widthHeight.length != 2) {
            System.err.println("File Read Error: start coords");
            return null;
        }
        Point start = new Point(
                Integer.parseInt(startCoords[0]),
                Integer.parseInt(startCoords[1])
        );

        line = scanner.nextLine();
        String[] endCoords = line.split("\\s");
        if (endCoords.length != 2) {
            System.err.println("File Read Error");
            return null;
        }
        Point end = new Point(
                Integer.parseInt(endCoords[0]),
                Integer.parseInt(endCoords[1])
        );
        if (end.getX() < 0 || end.getX() > width
        || end.getY() < 0 || end.getY() > height
        || start.getX() < 0 || start.getX() > width
        || start.getY() < 0 || start.getY() > height) {
            System.err.println("Start points not within bounds!");
            return null;
        }
        double[][] costMatrix = new double[height][width];
        for (int i=0; i < height; i++) {
            String[] row = scanner.nextLine().split("\\s");
            if (row.length != width) {
                System.err.println("ERROR: Row #"
                        +i+" is not the right length!");

            }
            for (int j=0; j < width; j++) {
                costMatrix[i][j] = Double
                        .parseDouble(
                          row[j]
                        );
            }
        }
        scanner.close();
        return DataFormatter.fromCostMatrix(costMatrix, start, end);
    }
}
